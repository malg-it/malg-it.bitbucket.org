<!DOCTYPE html>
<html lang="it">
<head>
<?php gen_head(); ?>
</head>
<body>
	<!-- NAV -->
	<?php gen_navbar(); ?>
	<!-- /NAV -->
	
	<!-- Breadcrumb -->
	<ol class="breadcrumb">
		<li class="active"><a href="#">Home</a></li>
	</ol>
	<!-- /Breadcrumb -->
	
	<!-- Carousel 1 -->
	<?php gen_carousel(
			array( // Genera automaticamente l'HTML del carousel.
				carousel_page("Milano, dic 27", loremIpsum(LI_FEATURESET_PLAIN), "http://pulpypics.com/wp-content/uploads/2014/08/animal-bird-animal-backgrounds-animal-bird-25907.jpg",
					array(
							carousel_button("Bottone 1", "#", true),
							carousel_button("Bottone 2", "#")
					)
				),
				carousel_page("Genova, gen 11", loremIpsum(LI_FEATURESET_PLAIN), "http://pulpypics.com/wp-content/uploads/2014/08/animal-bird-animal-backgrounds-animal-bird-25907.jpg",
					array(
							carousel_button("Bottone 1", "#", true),
							carousel_button("Bottone 2", "#")
					)
				),
			)); ?>
	<!-- /Carousel 1 -->

	
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="content">
					<h2>Incontro con $NAME</h2>
					<!-- Carousel 2
					================================================== -->
					<div id="myCarousel2" class="carousel slide" data-ride="carousel">
						<!-- Indicators -->
						<ol class="carousel-indicators">
							<li data-target="#myCarousel2" data-slide-to="0" class="active"></li>
							<li data-target="#myCarousel2" data-slide-to="1"></li>
							<li data-target="#myCarousel2" data-slide-to="2"></li>
						</ol>
						<div class="carousel-inner" role="listbox">
							<div class="item active">
								<img class="first-slide"
									src="http://pulpypics.com/wp-content/uploads/2014/08/animal-bird-animal-backgrounds-animal-bird-25907.jpg"
									alt="First slide">
								<div class="container">
									<div class="carousel-caption">Registrati sul nostro portale per
										rimanere sempre aggiornato</div>
								</div>
							</div>
							<div class="item">
								<img class="second-slide"
									src="http://randomwallpapers.net/fox-vixen-animal-animals-1920x1080-wallpaper65843.jpg"
									alt="Second slide">
								<div class="container">
									<div class="carousel-caption">Registrati sul nostro portale per
										rimanere sempre aggiornato</div>
								</div>
							</div>
							<div class="item">
								<img class="third-slide"
									src="http://cdn.playbuzz.com/cdn/279428ca-ddfa-45ce-87b5-53b20c6f3b38/ac4084b3-f55b-4332-83c9-0d411095e812.jpg"
									alt="Third slide">
								<div class="container">
									<div class="carousel-caption">Registrati sul nostro portale per
										rimanere sempre aggiornato</div>
								</div>
							</div>
						</div>
						<a class="left carousel-control" href="#myCarousel2" role="button"
							data-slide="prev"> <span class="glyphicon glyphicon-chevron-left"
							aria-hidden="true"></span> <span class="sr-only">Previous</span>
						</a> <a class="right carousel-control" href="#myCarousel2"
							role="button" data-slide="next"> <span
							class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						</a>
					</div>
					<!-- /.carousel -->
				</div>
			</div>
			<div class="col-md-6">
				<div class="content">
					<h2>MALG Industries</h2>
					<!-- Carousel 3
				================================================== -->
					<div id="myCarousel3" class="carousel slide" data-ride="carousel">
						<!-- Indicators -->
						<ol class="carousel-indicators">
							<li data-target="#myCarousel3" data-slide-to="0" class="active"></li>
							<li data-target="#myCarousel3" data-slide-to="1"></li>
							<li data-target="#myCarousel3" data-slide-to="2"></li>
						</ol>
						<div class="carousel-inner" role="listbox">
							<div class="item active">
								<img class="first-slide"
									src="http://cdn.playbuzz.com/cdn/279428ca-ddfa-45ce-87b5-53b20c6f3b38/ac4084b3-f55b-4332-83c9-0d411095e812.jpg"
									alt="First slide">
								<div class="container">
									<div class="carousel-caption">Avanguardia tecnologica al vostro
										servizio.</div>
								</div>
							</div>
							<div class="item">
								<img class="second-slide"
									src="http://cdn.playbuzz.com/cdn/279428ca-ddfa-45ce-87b5-53b20c6f3b38/ac4084b3-f55b-4332-83c9-0d411095e812.jpg"
									alt="Second slide">
								<div class="container">
									<div class="carousel-caption">Avanguardia tecnologica al vostro
										servizio.</div>
								</div>
							</div>
							<div class="item">
								<img class="third-slide"
									src="http://cdn.playbuzz.com/cdn/279428ca-ddfa-45ce-87b5-53b20c6f3b38/ac4084b3-f55b-4332-83c9-0d411095e812.jpg"
									alt="Third slide">
								<div class="container">
									<div class="carousel-caption">Avanguardia tecnologica al vostro
										servizio.</div>
								</div>
							</div>
						</div>
						<a class="left carousel-control" href="#myCarousel3" role="button"
							data-slide="prev"> <span class="glyphicon glyphicon-chevron-left"
							aria-hidden="true"></span> <span class="sr-only">Previous</span>
						</a> <a class="right carousel-control" href="#myCarousel3"
							role="button" data-slide="next"> <span
							class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						</a>
					</div>
					<!-- /.carousel -->
				</div>
			</div>
			<div class="col-md-6">
				<div class="content id="asa">
					<h2>Restiamo in contatto</h2>
					<img class="img-responsive" alt="contattaci" src="img/contact.jpg">
					<div class="descriz">Registrati sul nostro portale per rimanere
						sempre aggiornato</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="content">
					<h2>Diventa protagonista</h2>
					<img class="img-responsive" alt="Diventa protagonista"
						src="img/protagonist.png">
					<div class="descriz">Registra la tua azienda, dai visibilità ai
						tuoi prodotti</div>
				</div>
			</div>
		</div>

		<hr>
		<footer>
			<p>
				&copy; 2016 Company, Inc. &middot; <a href="#privacy">Privacy</a>
				&middot; <a href="#legal-info">Informazioni legali</a> &middot; <a
					href="#sitemap">Sitemap</a>
			</p>
		</footer>
	</div>
	<!-- /container -->



	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"
		type="text/javascript"></script>
	<script type="text/javascript">window.jQuery || document.write('<script src="js/vendor/jquery.min.js"><\/script>')</script>
	<script src="js/bootstrap.min.js" type="text/javascript"></script>


</body>
</html>

